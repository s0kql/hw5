﻿package com.company;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int num1 = getInt();
        int num2 = getInt();
        char operation = getOperation();
        int result = calc(num1,num2,operation);
        System.out.println("Ответ: "+result);
    }

    public static int getInt(){
        System.out.println("Введите число:");
        int num;
        if(scanner.hasNextInt()){
            num = scanner.nextInt();
        } else {
            System.out.println("НЕ ПОНЯЛ , ВВЕДИТЕ НОРМАЛЬНО ЧИСЛО, БУКАВЫ ЗАПРЕЩЕНЫ!!!");
            scanner.next();
            num = getInt();
        }
        return num;
    }

    public static char getOperation(){
        System.out.println("Введите знак операции (-, +, *, /):");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("НЕ ПОНЯЛ , ВВЕДИТЕ НОРМАЛЬНО ЧИСЛО!!!");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }

    public static int calc(int num1, int num2, char operation){
        int result;
        switch (operation){
            case '+':
                result = num1+num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1*num2;
                break;
            case '/':
                result = num1/num2;
                break;
            default:
                System.out.println("НЕ ПОНЯЛ , А МОЖНО НОРМАЛЬНО ВВЕСТИ ОПЕРАЦИЮ ???");
                result = calc(num1, num2, getOperation());
        }
        return result;
    }
}